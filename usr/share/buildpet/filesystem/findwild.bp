#!/bin/sh

PKG_NAME="findwild"
PKG_VER="1.2"
PKG_REV="1"
PKG_DESC="File searching tool"
PKG_CAT="Filesystem"
PKG_DEPS=""

download() {
	# download the sources
	[ -f $PKG_NAME-$PKG_VER.tar.gz ] && return 0
	wget --no-check-certificate http://kornelix.squarespace.com/storage/downloads/findwild-1.2.tar.gz
	[ $? -ne 0 ] && return 1
	return 0
}

build() {
	# extract the sources
	tar -xzvf $PKG_NAME-$PKG_VER.tar.gz
	[ $? -ne 0 ] && return 1

	cd $PKG_NAME-$PKG_VER

	# set the CFLAGS
	sed -i s/'-O3'/"$CFLAGS"/g Makefile
	[ $? -ne 0 ] && return 1
	
	# build the package
	make -j $BUILD_THREADS
	[ $? -ne 0 ] && return 1

	return 0
}

package() {
	# prevent the makefile from calling xdg-desktop menu
	sed -i s/'.*xdg-desktop-menu .*'//g Makefile
	[ $? -ne 0 ] && return 1
	
	# install the package
	make PREFIX=$INSTALL_DIR/usr install
	[ $? -ne 0 ] && return 1
	
	# move the license
	mkdir -p $INSTALL_DIR/usr/share/doc/legal/findwild
	[ $? -ne 0 ] && return 1
	mv $INSTALL_DIR/usr/share/doc/findwild/COPYING $INSTALL_DIR/usr/share/doc/legal/findwild
	[ $? -ne 0 ] && return 1
	
	# remove an unneeded directory
	rm -rf $INSTALL_DIR/usr/share/findwild/data
	[ $? -ne 0 ] && return 1
	
	# create a symlink to the icon
	mkdir $INSTALL_DIR/usr/share/pixmaps
	ln -s ../findwild/icons/findwild.png $INSTALL_DIR/usr/share/pixmaps/findwild.png
	[ $? -ne 0 ] && return 1
	
	# replace the menu entry
	rm -f $INSTALL_DIR/usr/share/applications/kornelix-findwild.desktop
	[ $? -ne 0 ] && return 1
	echo '[Desktop Entry]
Encoding=UTF-8
Name=Findwild search tool
Icon=findwild.png
Comment=Findwild search tool
Exec=findwild
Terminal=false
Type=Application
Categories=Utility;Filesystem;
GenericName=Findwild search tool' > $INSTALL_DIR/usr/share/applications/findwild.desktop
	[ $? -ne 0 ] && return 1
	chmod 644 $INSTALL_DIR/usr/share/applications/findwild.desktop
	[ $? -ne 0 ] && return 1
	
	return 0
}
